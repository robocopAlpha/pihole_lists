# Pihole Lists Manager

If you find this repo useful please consider giving it a star ⭐️ on gitlab.

[![PayPal](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://paypal.me/robocopAlpha)



## About the project

This repository contains scripts, blocklists and whitelists I use to manage the [gravity database](https://docs.pi-hole.net/core/pihole-command/#gravity) on my pihole instances.

The script `pull.lists.sh` fetches public blocklists and aggregates them with [my personal blocklist](blocked). Finally the blocklists are compressed (xz) and are ready to be used. 

> *The compressed blocklists save time & data spent in getting the blocklists over slow internet connections or while deploying to several machines.* This results in files that is somewhere between 11-25% of their original size.

The script `update.pi.sh` is run on the pihole instances (raspberrypi, pc, docker), and it fetches the compressed blocklists and [my whitelist](dump.whitelist.txt). The script adds my whitelisted domains to pihole and updates gravity on pihole using `pihole -g`.

## Note:

1) This script uses [`sd`](https://github.com/chmln/sd) and [`rg`](https://github.com/BurntSushi/ripgrep) as replacement for `sed` and `grep`. These time-tested utilities although very reliable are slower compared to their new age counterparts. If you have systems where they are not available, then you can edit the relevant lines in the `pull.lists.sh` to use `sed` and `grep`.

2) Currently `update.pi.sh` has this repo as upstream, but it can be changed to some other location, even locally if you’re running pihole on the same device.

## How to use:

Run the following lines to update your blocklists:

```sh
./pull.lists.sh
# ensure everything is looking good
# commit to git
git add -u ; git commit -m "Update `date +'%Y-%m-%d'`" ; git push
```

![Screen Shot 2020-07-29 at 13.24.15](assets/pulling.png)

Then updating your pihole instances is as simple as running the included `update.pi.sh` script on your pihole from your git repo:

```sh
curl -L "https://gitlab.com/robocopAlpha/pihole_lists/-/raw/main/update.pi.sh" | bash

# To make it even easier
# just add this to your ~/.bashrc or ~/.zshrc
alias update.pi='curl -L "https://gitlab.com/robocopAlpha/pihole_lists/-/raw/main/update.pi.sh" | bash'

# Command to easily add the alias is included in the update.pi.sh
```

## Adding as a cron job

You can add the update.pi script as a cron job to let the updates happen on a set time automatically by running (requires sudo rights):

```sh
sudo -s
ln -s /home/dietpi/pihole_lists/update.pi.sh /usr/local/bin/updatepi
chmod +x /usr/local/bin/updatepi
echo '5 4 * * *   root     PATH="$PATH:/usr/sbin:/usr/local/bin/" updatepi' >| /etc/cron.d/updatepi
exit
```

#### Adding List generation as a cron job

```sh
sudo -s
ln -s /home/dietpi/pihole_lists/pull.lists.sh /usr/local/bin/pullLists 
chmod +x /usr/local/bin/pullLists
echo '30 3 * * *   root     PATH="/home/linuxbrew/.linuxbrew/bin:/usr/sbin:/usr/local/bin/:$PATH" pullLists >| /home/dietpi/pihole_lists/cron_upd.log || cat /home/dietpi/pihole_lists/cron_upd.log' >| /etc/cron.d/pullLists
exit
```



## Whitelisting / False Positive reports

The list sources are listed in the file [get_blocklists.sh](get_blocklists.sh), you can report issues to them directly to fix it. I can only give you “hot fixes”, which address the issue but doesn’t actually fix it.

