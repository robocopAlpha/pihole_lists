#!/usr/bin/env bash

# Functions
fetch_hosts(){
   URL=$1
   FNAME=$2
   
   curl -sSL "${URL}" | rg -INv "^[#<]|localhost" | sd '\s+$' ''  | sd "^0\\.0\\.0\\.0 |^127\\.0\\.0\\.1 " "" | sd "\\#.*" "" >| ${FNAME}
}

fetch_blocklist(){
	URL=$1
	FNAME=$2
	curl -sSL "${URL}" | rg -INv "^[#<]|localhost" | sd '\s+$' '' | sd "\\#.*" "" >| ${FNAME}	
}


#--- Pick fastest cat command (based on my experience)
if command -v gcat > /dev/null 2>&1; then
	catCMD='gcat'   # GNU cat /usr/local/bin/gcat 
	# (To get run: brewlog insall coreutils)
elif alias mcat > /dev/null 2>&1; then
	catCMD='mcat'   # mcat is aliased to /bin/cat that ships with macOS
else
	catCMD=$(command -v cat)  # If none of the above are found then vanilla cat
fi


#--- Set parameters based on Distribution
if [ "$(uname)" == "Darwin" ]; then
# 	echo "Setting params for Darwin..."
# 	getMD5(){
# 		# usage: getMD5 'filename' > outfile
# 		echo "$(md5 -q ${1})"
# 	}
	HOME_DIR='/Users/deepankar/Library/CloudStorage/GoogleDrive-robocopalpha404@gmail.com/My Drive/Git/Gitlab.DC'
	RCloneConfig=/Users/deepankar/.config/rclone/rclone.conf
elif [ "$(uname)" == "Linux" ]; then
# 	echo "Setting params for Linux..."
#  	getMD5()
#  	{
#  		# usage: getMD5 'filename' > outfile
#  		echo "$(md5sum ${1} | cut -c -32)"
#  	}
	HOME_DIR=/home/dietpi
	RCloneConfig=/home/dietpi/.config/rclone/rclone.conf
else
	echo "Only MacOS and Linux are supported."
	exit 1
fi

WRK_DIR=${HOME_DIR}/pihole_lists

LOGFILE="${WRK_DIR}/buildList.log"
if [ ! -f "$LOGFILE" ] ; then
	touch "$LOGFILE"
fi