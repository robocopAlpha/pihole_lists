#!/usr/bin/env bash

# ------- How to use ------:
#  curl -L 'https://gitlab.com/robocopAlpha/pihole_lists/-/raw/main/update.pi.sh' | bash


# --------- Make alias ---- :
# echo "alias update.pi='curl -L "https://gitlab.com/robocopAlpha/pihole_lists/-/raw/main/update.pi.sh" | bash'" >> .bashrc

# echo "alias update.pi='curl -L "https://gitlab.com/robocopAlpha/pihole_lists/-/raw/main/update.pi.sh" | bash'" >> .zshrc


# ----- Begin Script ------

# creating directory if it doesn't exist.
if cd /home/dietpi/pihole > /dev/null ; then
	echo "[✓]homedir exists..."
else
	mkdir -p /home/dietpi/pihole
fi
# installing xz if it doesn't exit
 
if which xz > /dev/null ; then
	echo "[✓]package xz-utils found..."
else
	apt update; apt install xz-utils
fi

cd /home/dietpi/pihole

LOGFILE="/home/dietpi/pihole/update.log"

if [ ! -f "$LOGFILE" ] ; then
	# Creating brew.log
	echo "Creating $LOGFILE"
	mkdir -p "$(dirname "$LOGFILE")"
	touch "$LOGFILE"
fi

echo "pihole being udpdated on: $(date)" | tee -a "$LOGFILE"
curl -L 'https://www.dropbox.com/sh/oyjii6ndqduixt8/AADn57717lc0TE4AW5dxB0p4a?dl=1' --output ./blocklist.zip && unzip -o ./blocklist.zip -x /
rm ./blocklist.zip
xz -fd MyBlocklist.txt.xz
xz -fd facebook_block.txt.xz

curl -OJL 'https://gitlab.com/robocopAlpha/pihole_lists/-/raw/main/whitelist.txt'
grep "^[^#]" whitelist.txt | sort -u | uniq | tr '\r\n' ' ' >| whitelist
rm -f whitelist.txt
$(command -v pihole) -w --nuke > /dev/null 2>&1 | tee -a "$LOGFILE"
$(command -v pihole) -w -q -nr $(cat whitelist) 2>&1 | tee -a "$LOGFILE"
rm whitelist

$(command -v pihole) updateGravity 2>&1 | tee -a "$LOGFILE"

# Add these to pihole adlist 
# file:///home/dietpi/pihole/MyBlocklist.txt
# file:///home/dietpi/pihole/facebook_block.txt
