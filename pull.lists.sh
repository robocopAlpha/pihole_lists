#!/usr/bin/env bash

#--- Set parameters based on Distribution
if [ "$(uname)" == "Darwin" ]; then
	cd '/Users/deepankar/Library/CloudStorage/GoogleDrive-robocopalpha404@gmail.com/My Drive/Git/Gitlab.DC/pihole_lists' || echo "can't find ~/Git/Gitlab.DC/pihole_lists"
elif [ "$(uname)" == "Linux" ]; then
	cd '/home/dietpi/pihole_lists' || echo "/home/dietpi not found."
else
	echo "Only MacOS and Linux are supported."
	exit 1
fi

[[ -f set_env.sh ]] && source set_env.sh || { echo "set_env.sh not found"; exit 1; }

# Mounting storage
if df | grep -c dropbox > /dev/null; then
	umount "${HOME_DIR}/dropbox" || sudo umount "${HOME_DIR}/dropbox"
fi

if [[ -d "${HOME_DIR}/dropbox" ]]; then
	rm -rf "${HOME_DIR}/dropbox"
	mkdir "${HOME_DIR}/dropbox"
else 
	mkdir "${HOME_DIR}/dropbox"
fi
rclone mount --daemon dropbox:/Apps/rclone_DC/blocklist "${HOME_DIR}/dropbox" --volname=dropbox --config="${RCloneConfig}" || { echo "Failed to mount dropbox. Execution terminated!"; exit 1; }

cd "${WRK_DIR}/"
echo $(date +%d.%m.%y-%H:%M:%S) >| lastpull 
${catCMD} lastpull >> "${LOGFILE}"


# Fetch blocklists
bash "${WRK_DIR}/get_blocklists.sh"
if [ $? -ne 0 ]; then
	echo "Failed to get blocklists. Execution terminated!"
	exit 1
fi

# Block FB
source "${WRK_DIR}/block_FB.sh" || { echo "Failed to get Facebook blocklist. Execution terminated!"; exit 1; }


# Combining blocklists
echo "Consolidating blocklists..."
${catCMD} Mirrors/* | sort -u | uniq >| MyBlocklist.txt
wc -l MyBlocklist.txt

echo -e "\nAdding these to my own Blacklist: \n"
${catCMD} blocked | rg -vIN "^[\\#]" | sd '\s+$' '' | sort -u | uniq >> MyBlocklist.txt
echo -e "   $(wc -l MyBlocklist.txt) unique domains. \n" | tee -a "$LOGFILE"
echo -e "   $(wc -l facebook_block.txt) unique domains. \n" | tee -a "$LOGFILE"

#-------
# Clean-up
echo "Cleaning up..."
rm -rf Mirrors
rm -f blocklists/MyBlocklist.txt.xz blocklists/facebook_block.txt.xz
mkdir blocklists
mv MyBlocklist.txt blocklists/.
mv facebook_block.txt blocklists/.

#--------
# Archiving
echo "Compressing blocklists..."
cd "${WRK_DIR}/blocklists/"
# getMD5 "MyBlocklist.txt" >| MyBlocklist.checksum
# getMD5 "facebook_block.txt" >| facebook_block.checksum
sleep 1
xz -v -T4 MyBlocklist.txt
xz -v -T4 facebook_block.txt
chmod 777 ./*

#---------
# Moving to target
echo "Moving blocklists to destination..."
cd "${WRK_DIR}/"
rsync -a --progress blocklists/* "${HOME_DIR}/dropbox/" | tee -a "$LOGFILE"

echo "Final clean up..."
umount "${HOME_DIR}/dropbox" || sudo umount "${HOME_DIR}/dropbox"
rm -rf "${HOME_DIR}/dropbox"
rm -rf "blocklists"

echo "Done!"