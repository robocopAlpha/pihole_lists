#!/usr/bin/env bash
# Facebook & Whatsapp

[[ -f set_env.sh ]] && source set_env.sh || { echo "set_env.sh not found"; exit 1; }

rg -FINv "whatsapp" "${WRK_DIR}/Mirrors/facebook_block.txt" >| "${WRK_DIR}/Mirrors/tmp.txt"
rg -FIN "whatsapp" "${WRK_DIR}/Mirrors/facebook_block.txt" | rg -FIN "facebook" >> "${WRK_DIR}/Mirrors/tmp.txt"
rm -f "${WRK_DIR}/Mirrors/facebook_block.txt"
sort -u "${WRK_DIR}/Mirrors/tmp.txt" | uniq > "${WRK_DIR}/facebook_block.txt"
rm -f "${WRK_DIR}/Mirrors/tmp.txt"

